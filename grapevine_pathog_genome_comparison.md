# GENOME COMPARISONS TO GET MOLECULAR MARKER FOR FUNGAL TRUNK PATHOGENS DIAGNOSTIC TEST #
Using [LAST](http://last.cbrc.jp/) we want to make a wide-genome comparison with the aim to find common regions among pathogens, but different from the common microorganisms found as endophytes in grapevine, that could act as contaminants.

###Download LAST from [here](http://last.cbrc.jp/) and Install from source###
`make`
`sudo make install`

### Despues de decargar todos los genomas de hongos reconocidos como pat�genos de vid y los no patogenos pero huespedes comun de vid, realice la concatenaci�n mediante: ###
####concatenate all the pathogen genomes#####
`cat *s.fasta* > concat_pathogens.fasta.gz` 
`cat *fna* >> concat_pathogens.fasta.gz `
####concatenate the common grapevine habitant genomes as controls#####
`cat *ds.fasta* > concat_nopathogens.fasta.gz`

### descargar los archivos del [drive](https://drive.google.com/drive/folders/1TcWew9TcyqwTSFPtluzrRKHMnEpUpLCy?usp=sharing) y preparar la referencia en LAST mediante ###
`lastdb -cR01 cat_pathog concat_pathogens.fasta.gz`

###working on prepared fasta input files NO PATHOGENS###
###Prepare through maf_fasta.py###
###for T harzianum##
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare T_harzianum Trihar1_AssemblyScaffolds.fasta.gz -o Tri_harz_prepared.fasta.gz -t Names_Tri_harz.txt`
###for A alternata###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare A_alternata Altalt1_AssemblyScaffolds.fasta.gz -o Alt_alt_prepared.fasta.gz -t Names_Alt_alt.txt`
###for P chrysogenum###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare P_chrysogenum PenchWisc1_1_AssemblyScaffolds.fasta.gz -o Pen_chrys_prepared.fasta.gz -t Names_Pen_chrys.txt`

###Then to construct the LAST db for comparisons###
`for f in *prepared.fasta.gz; do lastdb -cR01 $(echo $f | cut -f1 -d ".") $f ; done`

###comparison T_harz vs A_alt##
`lastal Tri_harz_prepared Alt_alt_prepared.fasta.gz > Tharz_Aalt.maf`
###extract common region###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Tharz_Aalt.maf -o Tharz_Aalt.common.fasta.gz`
###Now compare to P_chrysogenum###
`lastal Pen_chrys_prepared Tharz_Aalt.common.fasta.gz > Tharz_Aalt_Pchr.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Tharz_Aalt_Pchr.maf -o Tharz_Aalt_Pchr.common.fasta.gz`
### construct a DB from the common regions in T_harzianum, A_alternata y P_chrysogenum ###
`lastdb -cR01 no_pathogens_db Tharz_Aalt_Pchr.common.fasta.gz`

###working on prepared fasta input files PATHOGENS###
###Prepare through maf_fasta.py###
###for L. theobromae ##
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare L_theobromae GCA_002111425.1_ASM211142v1_genomic.fna.gz -o Las_theob_prepared.fasta.gz -t Names_Las_theob.txt`
###for D. seriata ###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare D_seriata GCA_001006355.1_UCDDS831v0.1_genomic.fna.gz -o Dip_ser_prepared.fasta.gz -t Names_Dip_ser.txt`
###for N. parvum###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare N_parvum Neopa1_AssemblyScaffolds.fasta.gz -o Neo_par_prepared.fasta.gz -t Names_Neo_par.txt`
###for B. dothidea###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare B_dothidea Botdo1_1_AssemblyScaffolds.fasta.gz -o Bot_dot_prepared.fasta.gz -t Names_Bot_dot.txt`
###for E. lata###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare E_lata Eutla1_AssemblyScaffolds.fasta.gz -o Eut_lata_prepared.fasta.gz -t Names_Eut_lata.txt`
###for P. chlamydospora###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare P_chlamydospora Phach1_AssemblyScaffolds.fasta.gz -o Pha_chla_prepared.fasta.gz -t Names_Pha_chla.txt`
###for P. aleophilum###
`python /home/quetjaune/Documents/scripts/maf_fasta.py prepare P_aleophilum Phaal1_AssemblyScaffolds.fasta.gz -o Pha_aleo_prepared.fasta.gz -t Names_Pha_aleo.txt`

###Then to construct the LAST db for comparisons###
`for f in *prepared.fasta.gz; do lastdb -cR01 $(echo $f | cut -f1 -d ".") $f ; done`

###comparison L_theob vs D_ser##
`lastal Las_theob_prepared Dip_ser_prepared.fasta.gz > Ltheob_Dser.maf`
###extract common region###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser.maf -o Ltheob_Dser.common.fasta.gz`
###Now compare to N_parvum###
`lastal Neo_par_prepared Ltheob_Dser.common.fasta.gz > Ltheob_Dser_Npar.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser_Npar.maf -o Ltheob_Dser_Npar.common.fasta.gz`

 Trabaj� hasta ac� porque despu�s se generan archivos .maf muy grandes (hasta 52 GB)


###Now compare to B. dothidea###
`lastal Bot_dot_prepared Ltheob_Dser_Npar.common.fasta.gz > Ltheob_Dser_Npar_Bdot.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser_Npar_Bdot.maf -o Ltheob_Dser_Npar_Bdot.common.fasta.gz`
###Now compare to E. lata###
`lastal Eut_lata_prepared Ltheob_Dser_Npar_Bdot.common.fasta.gz > Ltheob_Dser_Npar_Bdot_Elat.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser_Npar_Bdot_Elat.maf -o Ltheob_Dser_Npar_Bdot_Elat.common.fasta.gz`
###Now compare to P. chlamydospora###
`lastal Pha_chla_prepared Ltheob_Dser_Npar_Bdot_Elat.common.fasta.gz > Ltheob_Dser_Npar_Bdot_Elat_Pchl.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser_Npar_Bdot_Elat_Pchl.maf -o Ltheob_Dser_Npar_Bdot_Elat_Pchl.common.fasta.gz`
###Now compare to P. aleophylum###
`lastal Pha_aleo_prepared Ltheob_Dser_Npar_Bdot_Elat_Pchl.common.fasta.gz > Ltheob_Dser_Npar_Bdot_Elat_Pchl_Pale.maf`
###extract common regions###
`python /home/quetjaune/Documents/scripts/maf_fasta.py common Ltheob_Dser_Npar_Bdot_Elat_Pchl_Pale.maf -o Ltheob_Dser_Npar_Bdot_Elat_Pchl_Pale.common.fasta.gz`


### PATHOGENs vs NO PATHOGENS COMPARISON ###
`lastal no_pathogens_db Ltheob_Dser_Npar.common.fasta.gz > pathogens_vs_nopathogens.maf`
#### extract UNCOMMON regions ####
`python /home/quetjaune/Documents/scripts/maf_fasta.py uncommon pathogens_vs_nopathogens.maf Tharz_Aalt_Pchr.common.fasta.gz Ltheob_Dser_Npar.common.fasta.gz`